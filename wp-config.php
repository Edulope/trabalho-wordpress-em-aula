<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'lacafeteria' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'admin' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'admin' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RX@pr|uvf !;xwS`+H^?.q_BB}_wrV~eU0[<(-E)uCN4wK]Yu%t5D7 pgOVx=q2L' );
define( 'SECURE_AUTH_KEY',  '[up^GIeZ?F-4vJwvze|3$Nzn%qp>#;McQcZgTDJ^AmVx/-}b>u.Eb#q<??U^bOS8' );
define( 'LOGGED_IN_KEY',    'pz..h9Ku*5H/MVRKIB?un_E)?.a{S)zl!Yv^5Z)(rj7#,oYW4f6PwMic^8BZFc| ' );
define( 'NONCE_KEY',        '`>Of;> xxI3hSBJnmzs ROn,X p41&bC&[TDbQ+W~pTLbN+hd#=K;q|DdEcxpe -' );
define( 'AUTH_SALT',        '-(/YaqNZ7imfW|tp6t+ZW<aV0Nm[3}?Ai,RY?Fzj_#*$P:y-av!tJ5~Qgc|<;5Tm' );
define( 'SECURE_AUTH_SALT', 'e7b8~d{)CoPqj/dB*?ij_T(P1m&_M{Ehd:u3%lxufJnd%dAm)0z.7^CtTU<OWU=Y' );
define( 'LOGGED_IN_SALT',   ',5@oS<#!=H0^1k=D<S~S~$x[HZ6>>d28:Do,$BBXC4~`!iA&j&Rcy+_Sd2X09~.+' );
define( 'NONCE_SALT',       '7(2>rRnRP#0`-;6T6i}=;Fv+n5?E1K-&eKX.b(wd{CQLpg}S^~]=wSiUqv(h?PIZ' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
