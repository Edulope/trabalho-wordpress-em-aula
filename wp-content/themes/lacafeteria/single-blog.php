<?php
// Template Name: Single Blog
?>

<?php get_header(); ?>
    <section class="header-post" style="background: linear-gradient( rgba(36, 36, 36, .4), rgba(36, 36, 36, .6) ), url('assets/img/post.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover; width: 100%; height: 300px;">
        <div class="main-msg">
            <p class="wow fadeInDown">Post</p>
        </div>
    </section>

    <section class="body-post">
        <div class="container">
            <div class="content">
                <p class="post-date"><?php the_field('data_do_artigo');?></p>
                <p><?php the_field('texto_artigo');?></p>
            </div>
            <a href="blog.html" class="btn">Leia mais</a>
        </div>
    </section>

	<?php get_footer(); ?>